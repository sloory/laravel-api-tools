<?php

namespace Sloory\LaravelApiTools\ApiClient;

use GuzzleHttp;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;

final class JsonApiClient
{
    public function __construct(Client $guzzleClient)
    {
        $this->guzzleClient = $guzzleClient;
        $this->responseParser = new JsonApiResponseParser(
            new ApiResponseParser()
        );
    }

    /**
     * @param $entryPoint
     * @param array $data
     * @return ApiResponse
     * @throws GuzzleHttp\Exception\GuzzleException
     * @throws WrongApiResponseException
     */
    public function post($entryPoint, array $data = [])
    {
        try {
            $response = $this->guzzleClient->post(
                $entryPoint,
                [
                    GuzzleHttp\RequestOptions::JSON => $data
                ]
            );
        } catch (BadResponseException $e) {
            $response = $e->getResponse();
        }

        $responseStatus = $response->getStatusCode();
        if ($responseStatus > 400) {
            throw new WrongApiResponseException(
                'Http status:' . $responseStatus
            );
        }

        return $this->responseParser->parse($response->getBody());
    }
}