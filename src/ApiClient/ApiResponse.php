<?php

namespace Sloory\LaravelApiTools\ApiClient;

class ApiResponse
{
    /**
     * @var array
     */
    private $response;

    public function __construct(array $response)
    {
        $this->response = $response;
    }

    public function __toString()
    {
        return json_encode($this->response);
    }
}