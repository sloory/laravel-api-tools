<?php

namespace Sloory\LaravelApiTools\ApiClient;

final class SuccessApiResponse extends ApiResponse
{
    public $result = null;
}