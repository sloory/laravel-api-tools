<?php

namespace Sloory\LaravelApiTools\ApiClient;

use Sloory\LaravelApiTools\Exceptions\CheckedException;

final class WrongApiResponseException extends CheckedException
{

}