<?php

namespace Sloory\LaravelApiTools\ApiClient;

final class ErrorApiResponse extends ApiResponse
{
    public $error_code = null;
    public $error_message = null;
}