<?php

namespace Sloory\LaravelApiTools\ApiClient;

final class ApiResponseParser
{
    public function parse(array $response): ApiResponse
    {
        if (
            !isset($response['success'])
            || !is_bool($response['success'])
        ) {
            throw new WrongApiResponseException(
                'Problem with "success" field'
            );
        }

        return
            $response['success']
                ? $this->parseSuccessResponse($response)
                : $this->parseErrorResponse($response);
    }

    private function parseSuccessResponse(array $responseData): SuccessApiResponse
    {
        $response = new SuccessApiResponse($responseData);
        if (isset($responseData['result'])) {
            $response->result = $responseData['result'];
        }

        return $response;
    }

    private function parseErrorResponse(array $responseData): ErrorApiResponse
    {
        $response = new ErrorApiResponse($responseData);
        if (
            isset($responseData['error'])
            && is_array($responseData['error'])
        ) {
            if (isset($responseData['error']['code'])) {
                $response->error_code = $responseData['error']['code'];
            };
            if (isset($responseData['error']['message'])) {
                $response->error_message = $responseData['error']['message'];
            };
        }

        return $response;
    }
}