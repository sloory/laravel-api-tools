<?php

namespace Sloory\LaravelApiTools\ApiClient;

final class JsonApiResponseParser
{
    private $apiParser;

    public function __construct(ApiResponseParser $apiParser)
    {
        $this->apiParser = $apiParser;
    }

    public function parse(string $response): ApiResponse
    {
        $arrayResponse = json_decode($response, true);
        if (0 !== json_last_error()) {
            throw new WrongApiResponseException(
                'json_decode error: ' . json_last_error_msg()
            );
        }

        return $this->apiParser->parse($arrayResponse);
    }
}