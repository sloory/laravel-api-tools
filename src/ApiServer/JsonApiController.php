<?php

namespace Sloory\LaravelApiTools\ApiServer;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

use Sloory\LaravelApiTools\Http\ControllerInterface;


final class JsonApiController implements ControllerInterface
{
    private  $inner;

    public function __construct(ApiControllerInterface $inner)
    {
        $this->inner = $inner;
    }

    public function handle(Request $request): Response
    {
        return
            response()
                ->json(
                    $this->inner->handle($request)->data()
                );
    }
}
