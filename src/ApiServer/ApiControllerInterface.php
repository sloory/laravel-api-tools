<?php

namespace Sloory\LaravelApiTools\ApiServer;

use Illuminate\Http\Request;
use Sloory\LaravelApiTools\ApiServer\Exceptions\ApiErrorsExceptionInterface;
use Sloory\LaravelApiTools\Responses\ApiResponseInterface;

interface ApiControllerInterface
{
    /**
     * @param Request $request
     * @return ApiResponseInterface
     *@throws ApiErrorsExceptionInterface
     */
    public function handle(Request $request): ApiResponseInterface;
}
