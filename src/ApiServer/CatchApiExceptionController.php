<?php

namespace Sloory\LaravelApiTools\ApiServer;

use Illuminate\Http\Request;

use Sloory\LaravelApiTools\ApiServer\Exceptions\ApiErrorsExceptionInterface;
use Sloory\LaravelApiTools\Http\ApiServer\ApiControllerInterface;
use Sloory\LaravelApiTools\Responses\ApiResponseInterface;


final class CatchApiExceptionController implements ApiControllerInterface
{
    private  $inner;

    public function __construct(ApiControllerInterface $inner)
    {
        $this->inner = $inner;
    }

    public function handle(Request $request): ApiResponseInterface
    {
        try {
            return $this->inner->handle($request);
        } catch (ApiErrorsExceptionInterface $e) {
            return new ApiResponse(
                [
                    'code' => $e->getCode(),
                    'message' => $e->getMessage()
                ]
            );
        }
    }
}
