<?php

namespace Sloory\LaravelApiTools\ApiServer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Contracts\Validation\Validator as ValidatorContract;

use Sloory\LaravelApiTools\ApiServer\ApiControllerInterface;
use Sloory\LaravelApiTools\ApiServer\Exceptions\ApiErrorsException;
use Sloory\LaravelApiTools\Http\Exceptions\RequestValidationException;
use Sloory\LaravelApiTools\Responses\ApiResponseInterface;
use Sloory\LaravelApiTools\Http\Validators\ValidatorRulesInterface;


final class ValidateRequestApiController implements ApiControllerInterface
{
    private  $inner;

    /**
     * @var ValidatorRulesAbstract
     */
    private  $validatorRules;

    public function __construct(
        ApiControllerInterface $inner, ValidatorRulesInterface $validator
    )
    {
        $this->inner = $inner;
        $this->validatorRules = $validator;
    }

    /**
     * @throws RequestValidationException
     */
    public function handle(Request $request): ApiResponseInterface
    {
        $validator = Validator::make(
            $request->all(), $this->validatorRules->rules()
        );

        if ($validator->fails()) {
            throw ApiErrorsException::createByValidator($validator);
        }

        return $this->inner->handle($request);
    }
}
