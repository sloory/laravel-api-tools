<?php

namespace Sloory\LaravelApiTools\ApiServer\Exceptions;

use Illuminate\Contracts\Validation\Validator as ValidatorContract;
use Sloory\LaravelApiTools\Exceptions\CheckedException;
use Sloory\LaravelApiTools\Exceptions\UncheckedException;
use Sloory\LaravelApiTools\Responses\ApiError;

final class ApiErrorsException extends CheckedException implements ApiErrorsExceptionInterface
{
    private $apiErrors;

    public function __construct(array $apiErrors)
    {
        parent::__construct();

        if (!$apiErrors[0] instanceof ApiError) {
            throw new UncheckedException();
        }

        $this->apiErrors = $apiErrors;
    }

    public static function createByError(ApiError $error): self
    {
        return new self([$error]);
    }

    public static function createByValidator(ValidatorContract $validator): self
    {
        $errors = [];
        foreach ($validator->errors()->all() as $error) {
            $apiError = new ApiError();
            $apiError->title = $error;
            $errors[] = $apiError;
        }

        return new self($errors);
    }

    public function getApiErrors(): array
    {
        return $this->apiErrors;
    }
}