<?php

namespace Sloory\LaravelApiTools\ApiServer\Exceptions;

interface ApiErrorsExceptionInterface
{
    public function getApiErrors(): array;
}