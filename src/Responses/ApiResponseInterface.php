<?php

namespace Sloory\LaravelApiTools\Responses;

interface ApiResponseInterface
{
    public function data(): array;
}