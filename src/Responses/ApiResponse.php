<?php

namespace Sloory\LaravelApiTools\Responses;

final class ApiResponse implements ApiResponseInterface
{
    private $data = null;

    public function __construct(array $data = null)
    {
        if ($data) {
            $this->data = $data;
        }
    }

    public function data(): array
    {
        return $this->data;
    }
}