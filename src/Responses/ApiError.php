<?php

namespace Sloory\LaravelApiTools\Responses;

final class ApiError
{
    public $code = null;
    public $title = null;
    public $detail = null;
}