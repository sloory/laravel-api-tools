<?php

namespace Sloory\LaravelApiTools\Http;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

interface ControllerInterface
{
    public function handle(Request $request): Response;
}
