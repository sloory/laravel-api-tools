<?php

namespace Sloory\LaravelApiTools\Http;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

final class LogRequestController implements ControllerInterface
{
    /**
     * @var ControllerInterface
     */
    private  $inner;

    private $label;

    public function __construct(ControllerInterface $inner, string $label = null)
    {
        $this->label = $label;
        $this->inner = $inner;
    }

    public function handle(Request $request): Response
    {
        \Log::info($this->label, ["request" => $request->all()]);

        return $this->inner->handle($request);
    }
}
