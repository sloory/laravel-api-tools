<?php

namespace Sloory\LaravelApiTools\Http;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;

use Sloory\LaravelApiTools\Http\Exceptions\RequestValidationException;
use Sloory\LaravelApiTools\Http\Validators\ValidatorRulesInterface;

final class ValidateRequestController implements ControllerInterface
{
    /**
     * @var ControllerInterface
     */
    private  $inner;

    /**
     * @var ValidatorRulesAbstract
     */
    private  $validatorRules;

    public function __construct(
        ControllerInterface $inner, ValidatorRulesInterface $validator
    )
    {
        $this->inner = $inner;
        $this->validatorRules = $validator;
    }

    /**
     * @throws RequestValidationException
     */
    public function handle(Request $request): Response
    {
        $validator = Validator::make(
            $request->all(), $this->validatorRules->rules()
        );

        if ($validator->fails()) {
            throw new RequestValidationException(
                $validator->errors()->all()
            );
        }

        return $this->inner->handle($request);
    }
}
