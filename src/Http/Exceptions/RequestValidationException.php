<?php

namespace Sloory\LaravelApiTools\Http\Exceptions;

use Sloory\LaravelApiTools\Exceptions\CheckedException;

class RequestValidationException extends CheckedException
{
    private $errors = [];

    public function __construct(array $errors)
    {
        parent::__construct();

        $this->errors = $errors;
    }
}
