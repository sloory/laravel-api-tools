<?php

namespace Sloory\LaravelApiTools\Http\Validators;

interface ValidatorRulesInterface
{
    public function rules(): array;
}