<?php
namespace ApiClient;

use GuzzleHttp\Client;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use Sloory\LaravelApiTools\ApiClient\ErrorApiResponse;
use Sloory\LaravelApiTools\ApiClient\JsonApiClient;
use Sloory\LaravelApiTools\ApiClient\SuccessApiResponse;
use Sloory\LaravelApiTools\ApiClient\WrongApiResponseException;

class JsonApiClientTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $clientRequests = [];
    
    protected function _before()
    {
        $this->clientRequests = [];
    }

    protected function _after()
    {
    }

    public function testForErrorReturnApiErrorResponse()
    {
        $response =
            $this->createMockedClient(
                    new Response(
                    200, [],
                    '
    {
        "success": false
    }'
                )
            )
                ->post('testEndPoint');

        $this->assertInstanceOf(ErrorApiResponse::class, $response);
    }

    public function testForIncorrectJsonResponseThrowException()
    {
        $this->expectException(WrongApiResponseException::class);

        $this->createMockedClient(
            new Response(
                200, [],
            'Not json at all "success": false }'
            )
        )
            ->post('entryPoint');
    }

    public function testWrongFormattedResponseThrowException()
    {
        $this->expectException(WrongApiResponseException::class);

        $this->createMockedClient(
            new Response(
                200, [],
                '
{
    "success_wrong": false
}'
            )
        )
            ->post('entryPoint');

    }

    public function testForErrorWithCodeReturnApiErrorResponseWithCode()
    {
        $response =
            $this->createMockedClient(
                new Response(
                    200, [],
                    '
{
    "success": false,
    "error": {
        "code": "wrong_supplier_id"
    }
}'
                )
            )
                ->post('entryPoint');

        $this->assertInstanceOf(ErrorApiResponse::class, $response);
        $this->assertEquals("wrong_supplier_id", $response->error_code);
    }

    public function testWithAnyHttpErrorStatusesThrowException()
    {
        $this->expectException(
            WrongApiResponseException::class
        );

        $this
            ->createMockedClient(new Response(404))
                ->post('entryPoint');
    }

    public function testForSuccessReturnSuccessApiResponseWithResult()
    {
        $response =
            $this->createMockedClient(
                new Response(
                    200, [],
                    '
{
    "success": true,
    "result": {
        "any_data": "data",
        "inner_array": {
            "item1": "1",
            "item2": "1"
        }
    }
}'
                )
            )
                ->post('entryPoint');

        $this->assertInstanceOf(SuccessApiResponse::class, $response);
        $this->assertEquals(
            [
                "any_data" => "data",
                "inner_array" => [
                    "item1" => "1",
                    "item2" => "1"
                ]
            ],
            $response->result
        );
    }

    public function testForCorrectRequestUrlAndRequest()
    {
        $data = [
            'id' => 11,
            'machineNumber' => 'M1'
        ];

        $response =
            $this->createMockedClient(
                new Response(
                    200, [], '{"success": true}'
                )
            )->post('photo-module', $data);

        $request = $this->clientRequests[0]['request'];

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals(
            'http://domain.ru/api/v1/photo-module',
            (string)$request->getUri()
        );
        $this->assertEquals(
            'application/json', $request->getHeader('Content-Type')[0]
        );

        $requestData = json_decode((string)$request->getBody(), true);

        $this->assertEquals($data, $requestData);
    }

    private function createMockedClient(Response $response): JsonApiClient
    {
        $mock = new MockHandler([$response]);

        $history = Middleware::history($this->clientRequests);

        $handlerStack = HandlerStack::create($mock);
        $handlerStack->push($history);

        return new JsonApiClient(
            new Client(
                [
                    'handler' => $handlerStack,
                    'base_uri' => 'http://domain.ru/api/v1/',
                    'auth' => [
                        'username',
                        'password'
                    ]
                ]
            )
        );
    }
}